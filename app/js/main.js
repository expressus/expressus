$('#menu-toggle').click(function(event){
    event.preventDefault();
    $('.sidebar-wrapper').toggleClass("slide-menu-left");
    if($('.burger').html() == 'X') {
      $('.burger').html('☰').css('color', '#075177');
    } else {
      $('.burger').html('X').css('color', '#075177');
    }
});
$(function($){
    var contents = $('.accordeon-content');
    var titles = $('.accordeon-title');
    titles.on('click',function(){
        var title = $(this);
        contents.filter(':visible').slideUp(function(){
            $(this).prev('.accordeon-title').removeClass('open');
        });

        var content = title.next('.accordeon-content');

        if (!content.is(':visible')) {
            content.slideDown(function(){title.addClass('open')});
        }
    });
});

$('.responsive').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    // autoplay: true,
    // autoplaySpeed: 2000,
    arrows: false,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 736,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: false
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                slickRemove: 6,
                dots: false
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});

$(window).resize(function(event){
    if($(document).width() < 500 && removeFired == false) {
        $('.responsive').slick('slickRemove',slideIndex - 6);
    }
});
$('.js-remove-slide').on('click', function() {
    $('.add-remove').slick('slickRemove',slideIndex - 1);
    if (slideIndex !== 0){
        slideIndex--;
    }
});
$(document).ready(function(){

    $('.tab-link').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('.tab-link').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
    })
});

////////////////POPUPS////////////////////

$(document).ready(function () {
    $(".btn_singin").click(function () {
        $(".popup-wrap").fadeIn()
    });
    $(".reg_button").click(function () {
        $(".popup-wrap_reg").fadeIn()
    });
    $(".feedback_popup").click(function () {
        $(".popup-wrap_feed").fadeIn()
    });
    $(".popup-close").click(function () {
        $( ".popup-wrap,.popup-wrap_reg,.popup-wrap_reset, .popup-wrap_feed").fadeOut()
    });
});

$(function() {
    // Calling Login Form
    $(".login_form").click(function() {
        $(".popup-wrap_reg").fadeOut();
        $(".popup-wrap_reset").fadeOut();
        $(".popup-wrap").fadeIn();
        return false;
    });

    // Calling Register Form
    $(".register_form").click(function() {
        $(".popup-wrap").fadeOut();
        $(".popup-wrap_reset").fadeOut();
        $(".popup-wrap_reg").fadeIn();
        return false;
    });

    // Calling reset Form
    $(".reset_form").click(function() {
        $(".popup-wrap").fadeOut();
        $(".popup-wrap_reg").fadeOut();
        $(".popup-wrap_reset").fadeIn();
        return false;
    });
});

// true - ВЫЗОВ, false - закрыть
// function popupControl(x) {
//   if(x) {
//     $('.page_container').css('filter', 'blur(10px)');
//     $('.bluralizer').css('display', 'block');
//     $('.popup').css('display', 'block');
//   } else {
//     $('.page_container').css('filter', 'none');
//     $('.bluralizer').css('display', 'none');
//     $('.popup').css('display', 'none');
//   }
// }

$('.bluralizer').click(function(){
  popupControl(false);
});

//ФУНКЦИЯ ДЛЯ ВЫЗОВА ПОДСКАЗКИ НА СТРАНИЦЕ ОФОРМЛЕНИЯ ЗАКАЗА
//НАЗВАНИЕ БЛОКА

function showBlock(x) {
  if(x) {
    $('.info_tracking_block').css('display', 'none');
    $('#tracking').css('display', 'block');
  } else {
    $('.info_tracking_block').css('display', 'none');
    $('#delivery').css('display', 'block');
  }
}

function calculate() {
  x = +$('input[name="length"]').val() * +$('input[name="width"]').val() * +$('input[name="height"]').val() / 6000;
  x = x * 100;
  x = Math.round(x);
  x = x / 100;
  $('input[name="result"]').attr('value', x);
}

$(document).ready(function() {
  
});

